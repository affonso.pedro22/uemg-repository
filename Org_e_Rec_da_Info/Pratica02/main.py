import glob
import re
from datetime import datetime
from pyexcel_ods import save_data

files = glob.glob("/home/pedro/Desktop/Git/uemg-repository/Org_e_Rec_da_Info/Pratica01/CRPC/*.htm")

d = datetime.now()
file = open('logs/log_' + d.strftime('%d_%m_%Y_%H_%M') + '.ods', 'w')

a = set()
words = []
wordsFiles = []
wordsFilesFinal = set()
wordsFinal = []
str1 = ""

for _file in files:
    f = open(_file, 'r', errors='ignore')
    x = f.read()
    x = re.split(r'\s|;|\.|,|"?"|\d|%|\?|:|\||!|&', x)
    for y in x:
        z = re.findall('>|<|\(|\)|=|"?"|-|\..|#|\+', y)
        if not z:
            if y != "":
                y = y.lower()
                a.add(y)
                words.append(y)
                wordsFiles.append([y, _file])
                print(y)
for w in a:
    a = words.count(w)
    for b in wordsFiles:
        if b[0] == w:
            wordsFilesFinal.add(b[1])
    for c in wordsFilesFinal:
        str1 += ", " + c
    wordsFinal.append([w, a, str1])
    str1 = ""
    wordsFilesFinal.clear()

wordsFinal.sort(key=lambda c: c[1], reverse=True)
save_data(file.name, wordsFinal)

print("")
print("Arquivo " + file.name + " preenchido com sucesso!")

run = True
k = set()
m = set()
while run:
    print("1 - Busca")
    print("outro para sair")
    ent = int(input())
    print(ent)
    if ent == 1:
        entrada1 = input("Digite uma frase:")
        entrada1 = re.split(' ', entrada1)
        print("")
        print("buscas OR")
        print("Todos os arquivos que as palavras pesquisadas aparecem são:")
        for palavra in entrada1:
            for b in wordsFiles:
                if b[0] == palavra:
                    print(b[0], b[1])

        print("")
        print("buscas AND")
        print("Todos os arquivos que as palavras pesquisadas aparecem juntas são:")
        for palavra in entrada1:
            for b in wordsFiles:
                if b[0] == palavra:
                    m.add(b[1])
        if len(m) == 1:
            print(entrada1, m)
        m.clear()

        print("")
        print("buscas NOT")
        print("Todos os arquivos que as palavram pesquisadam não aparecem são:")
        for palavra in entrada1:
            for b in wordsFiles:
                if b[0] != palavra:
                    k.add(b[1])
            print(palavra, k)
            k.clear()
        print("")

    else:
        run = False
