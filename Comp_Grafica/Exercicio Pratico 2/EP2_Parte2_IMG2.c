#include<stdlib.h>
#include<GL/glut.h>

void Inicializa (void) {
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glutInitWindowSize(400,400);
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(-20.0,20.0,20.0,-20.0);
}

void Display(void) {

    float ang;
  
    glClear(GL_COLOR_BUFFER_BIT);
    
    glScalef(1.3f, 1.3f, 1.3f);
    
    glColor3f(1.0f, 0.3, 0.3f);
    glBegin(GL_TRIANGLES);
    glVertex2f(0.0f,0.0f);
    glVertex2f(-5.0f,10.0f);
    glVertex2f(5.0f,10.0f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(5.0f,10.0f);
    glVertex2f(10.0f,0.0f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(10.0f,0.0f);
    glVertex2f(5.0f,-10.0f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(5.0f,-10.0f);
    glVertex2f(-5.0f,-10.0f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(-5.0f,-10.0f);
    glVertex2f(-10.0f,0.0f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(-10.0f,0.0f);
    glVertex2f(-5.0f,10.0f);
    glEnd();

    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_LINE_STRIP);
    glVertex2f(0.0f,0.0f);
    glVertex2f(-5.0f,10.0f);
    glVertex2f(5.0f,10.0f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(5.0f,10.0f);
    glVertex2f(10.0f,0.0f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(10.0f,0.0f);
    glVertex2f(5.0f,-10.0f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(5.0f,-10.0f);
    glVertex2f(-5.0f,-10.0f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(-5.0f,-10.0f);
    glVertex2f(-10.0f,0.0f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(-10.0f,0.0f);
    glVertex2f(-5.0f,10.0f);
    glEnd();
    glFlush();
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutCreateWindow("IMG 2");
    glutDisplayFunc(Display);
    Inicializa();
    glutMainLoop();
}
