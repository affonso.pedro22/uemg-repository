#include<stdlib.h>
#include<GL/glut.h>

void Inicializa (void) {
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glutInitWindowSize(400,400);
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(-20.0,20.0,-20.0,20.0);
}

void Display(void) {

    float ang;

    glClear(GL_COLOR_BUFFER_BIT);
    glScalef(0.6f, 0.6f, 0.6f);
    
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_TRIANGLES);

    glVertex2f(0.0f,-10.0f);
    glVertex2f(10.0f,0.0f);
    glVertex2f(25.0f,-25.0f);
    glVertex2f(0.0f,-10.0f);
    glVertex2f(-10.0f,0.0f);
    glVertex2f(-25.0f,-25.0f);
    glVertex2f(-10.0f,-0.0f);
    glVertex2f(10.0f,0.0f);
    glVertex2f(0.0f,25.0f);
    glEnd();

    glColor3f(0.0f, 0.0f, 0.0f);

    glBegin(GL_LINE_STRIP);
    glColor3f(0.0f, 0.0f, 1.0f);
    //glVertex2f(10.0f,-10.0f);
    glVertex2f(10.0f,0.0f);
    glVertex2f(25.0f,-25.0f);
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex2f(0.0f,-10.0f);
    glVertex2f(-25.0f,-25.0f);
    glVertex2f(-25.0f,-25.0f);
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex2f(-10.0f,-0.0f);
    glVertex2f(0.0f,25.0f);
    glVertex2f(10.0f,0.0f);

    glEnd();
    glFlush();
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutCreateWindow("IMG 3");
    glutDisplayFunc(Display);
    Inicializa();
    glutMainLoop();
}
