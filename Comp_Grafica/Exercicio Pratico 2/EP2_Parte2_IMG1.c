#include <stdlib.h>
#include <GL/glut.h>

float offset_x = 0 ,offset_y = 0, rot = 0;

void Inicializa (void)
{
    // Define a cor de fundo da janela de visualização 
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    // Define a janela de visualização 2D
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0.0,20.0,0.0,20.0);
}

void Amarelo()
{
    glBegin(GL_QUADS);
        glColor3f(1,1,0);
        glVertex3f(3.0,8.0,0);
        glVertex3f(1.0,10.0,0);
        glVertex3f(3.0,12.0,0);
        glVertex3f(5.0,10.0,0);
    glEnd();
}

void Verde()
{
    glBegin(GL_QUADS);
        glColor3f(0,1,0);
        glVertex3f(10.0,8.0,0);
        glVertex3f(8.0,10.0,0);
        glVertex3f(10.0,12.0,0);
        glVertex3f(12.0,10.0,0);
    glEnd();
}

void Preto()
{
    glBegin(GL_QUADS);
        glColor3f(0,0,0);
        glVertex3f(10.0,1.0,0);
        glVertex3f(8.0, 3.0,0);
        glVertex3f(10.0,5.0,0);
        glVertex3f(12.0,3.0,0);
    glEnd();
}

void Vermelho()
{
    glBegin(GL_QUADS);
        glColor3f(1,0,0);
        glVertex3f(3.0,1.0,0);
        glVertex3f(1.0, 3.0,0);
        glVertex3f(3.0,5.0,0);
        glVertex3f(5.0,3.0,0);
    glEnd();
}

//Função callback de redesenho da janela de visualização
void display(void)
{
    glClearColor(0.0f, 0.0f, 1.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glScalef(1.5f, 1.5f, 1.5f);
    Vermelho();
    Preto();
    Verde();
    Amarelo();
    glFlush();
}


// Programa Principal 
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); 
    glutInitWindowSize(400,400);
    glutCreateWindow("IMG 1"); 
    glutDisplayFunc(display);
    Inicializa(); 
    glutMainLoop();
    return 0;
}
