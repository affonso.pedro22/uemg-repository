#include <stdlib.h>
#include <GL/glut.h>

// TECLAS: SETAS (CIMA,BAIXO,ESQUERDA DIREITA)
// ROTAÇÃO: PAGE_UP, PAGE_DOWN

float offset_x = 0 ,offset_y = 0, rot = 0;

void Inicializa (void)
{
    // Define a cor de fundo da janela de visualização 
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    // Define a janela de visualização 2D
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0.0,12.0,0.0,12.0);
}

void Telhado(float x, float y)
{
	glBegin(GL_TRIANGLES);
        glColor3f(1,0,0);
        glVertex3f(3.0 + x,6.0 + y,0);
        glVertex3f(6.0 + x, 9.0 + y,0);
        glVertex3f(9.0 + x,6.0 + y,0);
	glEnd();
}

void Base(float x, float y)
{
    glBegin(GL_QUADS);
        glColor3f(0,0,1);
        glVertex3f(3.0 + x,1.0 + y,0);
        glVertex3f(9.0 + x, 1.0 + y,0);
        glVertex3f(9.0 + x,6.0 + y,0);
        glVertex3f(3.0 + x,6.0 + y,0);
    glEnd();
}

void Porta(float x, float y)
{
    glBegin(GL_QUADS);
        glColor3f(1,1,1);
        glVertex3f(5.0 + x,1.1 + y,0);
        glVertex3f(7.0 + x, 1.1 + y,0);
        glVertex3f(7.0 + x,4.0 + y,0);
        glVertex3f(5.0 + x,4.0 + y,0);
    glEnd();
}

void Janela(float x, float y)
{
    glBegin(GL_QUADS);
        glColor3f(1,1,1);
        glVertex3f(7.5 + x,3.0 + y,0);
        glVertex3f(8.5 + x, 3.0 + y,0);
        glVertex3f(8.5 + x,4.0 + y,0);
        glVertex3f(7.5 + x,4.0 + y,0);
    glEnd();
}

//Função callback de redesenho da janela de visualização
void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    //glScalef(0.8f, 0.8f, 0.8f);
    glPushMatrix();
    glRotatef(rot, 0.0f, 0.0f, 1.0f);
      Telhado(offset_x,offset_y);
      Base(offset_x,offset_y);
      Porta(offset_x,offset_y);
      Janela(offset_x,offset_y);
    glPopMatrix();
    glFlush();
    glutPostRedisplay();
    glutSwapBuffers();
}

void Teclado (int key, int x, int y)
{
    switch(key){
    case GLUT_KEY_LEFT: //left
	offset_x = offset_x - 1.5;
	glutPostRedisplay();
    	break;
    case GLUT_KEY_RIGHT: // right
	offset_x = offset_x + 1.5;
	glutPostRedisplay();
    	break;
    case GLUT_KEY_UP: // up
    	offset_y = offset_y + 1.5;
    	glutPostRedisplay();
    	break;
    case GLUT_KEY_DOWN: // down
    	offset_y = offset_y - 1.5;
    	glutPostRedisplay();
    	break;
    case GLUT_KEY_PAGE_UP: // up
    	rot = rot + 10;
    	glutPostRedisplay();
    	break;
    case GLUT_KEY_PAGE_DOWN: // down
    	rot = rot -10;
    	glutPostRedisplay();
    	break;
    }
}

// Programa Principal 
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB); 
    glutInitWindowSize(400,400);
    glutCreateWindow("Exercicio6");
    glutDisplayFunc(display);
    glClearColor(0, 0, 0, 0);
    gluOrtho2D(0.0,12.0,0.0,12.0);
    glutSpecialFunc(Teclado);
    glutMainLoop();
    return 0;
}
