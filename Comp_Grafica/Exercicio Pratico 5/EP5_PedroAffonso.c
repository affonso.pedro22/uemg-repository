/*
 * CS 520: environment-map.c 
 * Demonstrates the creation of environment map using the command
 * glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 0, 0, 500, 500, 0);
 * The scene can be rotated by pressing keys 'x', 'X', 'y', 'Y', 'z', 'Z'.
 * @Author: T.L. Yu, 2008F 
 *
 */

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>

int window;
int anglex= 0, angley = 0, anglez = 0;		//rotation angles
float r_spec = 0 , g_spec = 0, b_spec = 0, r_difu = 0, g_difu = 0, b_difu = 0;

void Defineluminacao(void) 
{
   GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
   GLfloat mat_diffuse[] = { 0.1, 0.5, 0.8, 1.0 };
   float light_specular[4] = { r_spec, g_spec, b_spec, 1.0 };     // r, g, b, a
   float light_diffuse[4] = { r_difu, g_difu, b_difu, 1.0 };     // r, g, b, a
   float light_position[4] = { 1.0, 1.0, 1.0 , 0.0 };   // x, y, z, w 
   
   glEnable(GL_LIGHTING);
   glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
   glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
   glMaterialf(GL_FRONT, GL_SHININESS, 25.0);
   glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
   glLightfv(GL_LIGHT0, GL_POSITION, light_position);
   glEnable(GL_LIGHT0);
}

void init(void)
{    
   glClearColor (0.9, 0.9, 0.8, 0.0);
   glShadeModel(GL_FLAT);
   glEnable(GL_DEPTH_TEST);
   
   // gera coordenadas de textura automaticamente
   glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
   glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
   glEnable(GL_TEXTURE_GEN_S);
   glEnable(GL_TEXTURE_GEN_T);

   //encapsulamento
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
   //filtro
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   //habilita textura
   glEnable(GL_TEXTURE_2D);
}

//rotate the objects
void rotate()
{
   glRotatef( anglex, 1.0, 0.0, 0.0);			
   glRotatef( angley, 0.0, 1.0, 0.0);			
   glRotatef( anglez, 0.0, 0.0, 1.0);			
}

//arbitrarily draw something
void drawScene()
{
   glDisable(GL_LIGHTING);
   glDisable(GL_TEXTURE_2D);
   glColor3f ( 0.1, 0.1, 0.8 );
   //draw a blue teapot at left side
   glPushMatrix();
   glTranslatef( -1.0, -1.0, 2.5 );
   glRotatef( 45, 1, 1, 1 );
   glPopMatrix(); 
   glEnable(GL_TEXTURE_2D);
   glEnable(GL_LIGHTING);
}

void display(void)
{
   Defineluminacao();
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   //Ajusta valores de cor e iluminação com a imagem de textura
   glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
   
   glLoadIdentity();
   gluLookAt ( 0, 0, 0, 5, 0, 0, 0, 0, 1 );	//put camera at center of origin
   glScalef ( 1, -1, 1 );			//flip left-right 
   rotate();
   drawScene();
   
   //Usa a imagem do framebuffer atual como textura (o teapot)
   //void glCopyTexImage2D(	GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border);
   glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 0, 0, 500, 500, 0);

   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glLoadIdentity();
   //put camera back at the observational point
   gluLookAt ( 5, 0, 0, 0, 0, 0, 0, 0, 1 );	
   rotate();
   drawScene();
   glutSolidTorus(1.0, 2.0, 10, 40);	// draw a sphere with this sphere map
   glFlush();
}

void keyboard(unsigned char key, int x, int y)
{
  switch(key) {
    case 'x':
      anglex = ( anglex + 3 ) % 360;
      break;
    case 'X':
      anglex = ( anglex - 3 ) % 360;
      break;
    case 'y':
      angley = ( angley + 3 ) % 360;
      break;
    case 'Y':
      angley = ( angley - 3 ) % 360;
      break;
    case 'z':
      anglez = ( anglez + 3 ) % 360;
      break;
    case 'Z':
      anglez = ( anglez - 3 ) % 360;
      break;
    case 'r':
      anglex = angley = anglez = 0;
      break;
    case 'a':
      r_difu = r_difu + 0.1;
      break;
    case 's':
      g_difu = g_difu + 0.1;
      break;
    case 'd':
      b_difu = b_difu + 0.1;
      break;
    case 'A':
      r_difu = r_difu - 0.1;
      if (r_difu < 0)
      {
      	r_difu = 0;
      }
      break;
    case 'S':
      g_difu = g_difu - 0.1;
      if (g_difu < 0)
      {
      	g_difu = 0;
      }
      break;
    case 'D':
      b_difu = b_difu - 0.1;
      if (b_difu < 0)
      {
      	b_difu = 0;
      }
      break;
    case 27: /* escape */
        glutDestroyWindow(window);
        exit(0);
  }
  glutPostRedisplay();
}

void reshape(int w, int h)
{
   glViewport(0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
    glOrtho(-4.0, 4.0, -4.0 * (GLfloat) h / (GLfloat) w,
            4.0 * (GLfloat) h / (GLfloat) w, -10.0, 10.0);

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   gluLookAt ( 5, 0, 0, 0, 0, 0, 0, 0, 1 );
}


int main(int argc, char** argv)
{
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
   glutInitWindowSize(500, 500);
   glutInitWindowPosition(100, 100);
   window = glutCreateWindow("Exercicio Pratico 5");
   init();
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   glutMainLoop();
   return 0; 
}
