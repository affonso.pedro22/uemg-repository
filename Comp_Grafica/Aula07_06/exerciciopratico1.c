#include<stdlib.h>
#include <GL/glut.h>

void display(void)
{
    gluOrtho2D(0.0, 20.0, 0.0, 20.0);
    glClear (GL_COLOR_BUFFER_BIT);
    glColor3f (0.3, 0.3, 0.3);
    glBegin(GL_POLYGON);
        // glVertex3f (5.0, 5.0, 0.0);
	glVertex3f (15.0, 5.0, 0.0);
	glVertex3f (15.0, 15.0, 0.0);
	glVertex3f (5.0, 15.0, 0.0);
    glEnd();
    glFlush ();
}

void init (void) 
{
    glClearColor (0.59, 0.20, 0.59, 0.0);
    glutInitWindowSize (250, 250); 
    glMatrixMode(GL_PROJECTION);
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
    glutCreateWindow ("Exercicio 1");
    init ();
    glutDisplayFunc(display); 
    glutMainLoop();
    return 0;  
}
