#include<stdlib.h>
#include<GL/glut.h>

void Inicializa (void)
{
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    // Define a janela de visualização 2D
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(-50.0,50.0,-50.0,50.0);
}

void Desenha(void)
{
    //Limpa a janela de visualização com a cor de fundo especificada
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_TRIANGLE_STRIP);
	glVertex2f(-35.0f,-14.0f);
	glVertex2f(-21.0f,14.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-7.0f,-14.0f);
	glVertex2f(7.0f,14.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2f(21.0f,-14.0f);
	glVertex2f(35.0f,14.0f);
    glEnd();
    glColor3f(0.0f, 0.0f, 1.0f);
    glBegin(GL_LINE_STRIP);	
	glVertex2f(-35.0f,-14.0f);
	glVertex2f(-21.0f,14.0f);
 	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-7.0f,-14.0f);
	glVertex2f(7.0f,14.0f);
 	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(21.0f,-14.0f);
	glVertex2f(35.0f,14.0f);
    glEnd();
    glFlush();
}

// Programa Principal
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutCreateWindow("Exercicio 2");
    glutDisplayFunc(Desenha);
    Inicializa();
    glutMainLoop();
}

