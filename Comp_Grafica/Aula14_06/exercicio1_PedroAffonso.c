#include<stdlib.h>
#include<GL/glut.h>

void Inicializa (void)
{
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(-500.0,500.0,-500.0,500.0);
}

//Stroke
void DesenhaTextoStroke(void *font, char *string){
  while(*string)
	glutStrokeCharacter(GLUT_STROKE_ROMAN, *string++);
}

void Desenha(void)
{
      glClear(GL_COLOR_BUFFER_BIT);
      glColor3f(0,0,0);
      glLineWidth(1);
      glScalef(0.55f, 0.55f, 0.55f);
      glTranslatef(5.0f, 5.0f, 0.0f);
      glRotatef(60.0, 0.0f, 0.0f, 1.0f);
      DesenhaTextoStroke(GLUT_STROKE_ROMAN,"abcdefghij");
      glFlush();
}

// Programa Principal
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutCreateWindow("Exercicio 1");
    glutInitWindowSize(400,400);
    glutDisplayFunc(Desenha);
    Inicializa();
    glutMainLoop();
}

