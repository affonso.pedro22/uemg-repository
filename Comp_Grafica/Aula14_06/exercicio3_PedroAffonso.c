#include <stdlib.h>
#include <GL/glut.h>
    
void Inicializa(void) 
{
	glClearColor(1.0f, 1.0f, 0.0f, 1.0f);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(-50.0f, 50.0f, -50.0f, 50.0f);
} 
    
void Desenha(void) 
{
	glClear(GL_COLOR_BUFFER_BIT);
	glEnable(GL_LINE_STIPPLE);
	glColor3f(1.0f, 0.0f, 0.0f);
	glLineWidth(5);
	glLineStipple(1, 0xFFFF);
	glBegin(GL_LINES);
	    glVertex2f(-20.0f, 20.0f);
	    glVertex2f(20.0f, 20.0f);
	    glVertex2f(-20.0f, -20.0f);
	    glVertex2f(20.0f, -20.0f);
	    glVertex2f(20.0f, -20.0f);
	    glVertex2f(20.0f, 20.0f);
	    glVertex2f(-20.0f, -20.0f);
	    glVertex2f(-20.0f, 20.0f);
        glVertex2f(-50.0f, 0.0f);
        glVertex2f(-2.5f, 0.0f);
        glVertex2f(2.5f, 0.0f);
        glVertex2f(50.0f, 0.0f);
        glVertex2f(0.0f, -50.0f);
        glVertex2f(0.0f, -2.5f);
        glVertex2f(0.0f, 2.5f);
        glVertex2f(0.0f, 50.0f);
	glEnd();
	glFlush();
}


int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutCreateWindow("Exercicio 3");
    glutDisplayFunc(Desenha);
    Inicializa();
    glutMainLoop();
}

