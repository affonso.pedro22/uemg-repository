#include <stdlib.h>
#include <GL/glut.h>


void Inicializa (void)
{
    // Define a cor de fundo da janela de visualização 
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    // Define a janela de visualização 2D
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0.0,12.0,0.0,12.0);
}

void Telhado()
{
	glBegin(GL_TRIANGLES);
        glColor3f(1,0,0);
        glVertex3f(3.0,6.0,0);
        glVertex3f(6.0, 9.0,0);
        glVertex3f(9.0,6.0,0);
	glEnd();
}

void Base()
{
    glBegin(GL_QUADS);
        glColor3f(0,0,1);
        glVertex3f(3.0,1.0,0);
        glVertex3f(9.0, 1.0,0);
        glVertex3f(9.0,6.0,0);
        glVertex3f(3.0,6.0,0);
    glEnd();
}

//Função callback de redesenho da janela de visualização
void DesenhaCasa(void)
{
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glScalef(0.8f, 0.8f, 0.8f);
    glPushMatrix();
        glTranslatef(8.0f, 10.0f, 0.0f); 
        Telhado();
    glPopMatrix();
    glPushMatrix();
        glRotatef(20, 0.0f, 0.0f, 1.0f);
        Base();
    glPopMatrix();
    glFlush();
}

// Programa Principal 
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); 
    glutInitWindowSize(400,400);
    glutCreateWindow("Exercicio6"); 
    glutDisplayFunc(DesenhaCasa);
    Inicializa(); 
    glutMainLoop();
    return 0;
}
