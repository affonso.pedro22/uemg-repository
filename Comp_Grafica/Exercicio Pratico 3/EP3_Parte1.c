#include <stdlib.h>
#include <GL/glut.h>

GLfloat angle, fAspect;
GLfloat deslocamentoX, deslocamentoY, deslocamentoZ;
float offset_x = 0 ,offset_y = 0, rot = 0;

void Telhado(float x, float y)
{
	glBegin(GL_TRIANGLES);
        glColor3f(1,0,0);
        glVertex3f(3.0 + x,6.0 + y,0);
        glVertex3f(6.0 + x, 9.0 + y,0);
        glVertex3f(9.0 + x,6.0 + y,0);
	glEnd();
}

void Base(float x, float y)
{
    glBegin(GL_QUADS);
        glColor3f(0,0,1);
        glVertex3f(3.0 + x,1.0 + y,0);
        glVertex3f(9.0 + x, 1.0 + y,0);
        glVertex3f(9.0 + x,6.0 + y,0);
        glVertex3f(3.0 + x,6.0 + y,0);
    glEnd();
}

void Porta(float x, float y)
{
    glBegin(GL_QUADS);
        glColor3f(1,1,1);
        glVertex3f(5.0 + x,1.1 + y,0);
        glVertex3f(7.0 + x, 1.1 + y,0);
        glVertex3f(7.0 + x,4.0 + y,0);
        glVertex3f(5.0 + x,4.0 + y,0);
    glEnd();
}

void Janela(float x, float y)
{
    glBegin(GL_QUADS);
        glColor3f(1,1,1);
        glVertex3f(7.5 + x,3.0 + y,0);
        glVertex3f(8.5 + x, 3.0 + y,0);
        glVertex3f(8.5 + x,4.0 + y,0);
        glVertex3f(7.5 + x,4.0 + y,0);
    glEnd();
}

void Desenha(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(0.0f, 0.0f, 0.0f);


	glClear(GL_COLOR_BUFFER_BIT);
	glPushMatrix();
	glRotatef(rot, 0.0f, 0.0f, 1.0f);
	  Telhado(offset_x,offset_y);
	  Base(offset_x,offset_y);
	  Porta(offset_x,offset_y);
	  Janela(offset_x,offset_y);
	glPopMatrix();
	glFlush();
	glutPostRedisplay();
	glutSwapBuffers();
	glFlush();
}

void EspecificaParametrosVisualizacao(void)
{
	glMatrixMode(GL_PROJECTION);
	
	glLoadIdentity();

	gluPerspective(angle,fAspect,0.5,500);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	gluLookAt(0+deslocamentoX,0+deslocamentoY,150+deslocamentoZ, 
		0+deslocamentoX,0+deslocamentoY,0+deslocamentoZ, 
		0,1,0);
}

void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	if ( h == 0 ) h = 1;


	glViewport(0, 0, w, h);

	fAspect = (GLfloat)w/(GLfloat)h;

	EspecificaParametrosVisualizacao();
}

void TeclasEspeciais(int key, int x, int y)
{
	switch(key)
	{
		case GLUT_KEY_UP:
			deslocamentoY -= 2;
			break;
		case GLUT_KEY_DOWN:
			deslocamentoY += 2;
			break;
		case GLUT_KEY_LEFT:
			deslocamentoX += 2;
			break;
		case GLUT_KEY_RIGHT:
			deslocamentoX -= 2;
			break;
		case GLUT_KEY_PAGE_UP:
			rot += 30;
			break;
		case GLUT_KEY_PAGE_DOWN:
			rot -= 30;
		break;
	}
	EspecificaParametrosVisualizacao();
	glutPostRedisplay();
}

void GerenciaMouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
		if (state == GLUT_DOWN) {
			if (angle >= 10) angle -= 5;
		}
	if (button == GLUT_RIGHT_BUTTON)
		if (state == GLUT_DOWN) {
			if (angle <= 100) angle += 5;
		}
	EspecificaParametrosVisualizacao();
	glutPostRedisplay();
}

void Teclado (unsigned char key, int x, int y) 
{
	if (key == 27)
		exit(0);
}

void Inicializa (void)
{   
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	angle=30;

	deslocamentoX = 0.0f;
	deslocamentoY = 0.0f;
	deslocamentoZ = 0.0f;
}

// Programa Principal 
int main(int argc, char **argv) 
{
   

    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); 

	glutInitWindowPosition(5,5); 

	glutInitWindowSize(450,450); 

	glutCreateWindow("Exercicio 1");

	glutDisplayFunc(Desenha);

	glutReshapeFunc(AlteraTamanhoJanela);

	glutSpecialFunc(TeclasEspeciais); 
 
	glutMouseFunc(GerenciaMouse);

	glutKeyboardFunc (Teclado);

	Inicializa();

	glutMainLoop();

	return 0;
}

