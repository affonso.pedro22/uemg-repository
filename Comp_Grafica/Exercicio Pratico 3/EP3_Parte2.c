#include <stdlib.h>
#include <GL/glut.h>

GLfloat fAspect, angX, angY, angZ;

void Desenha(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(1.0f, 1.0f, 1.0f);

	glPushMatrix();

	glRotatef(angX, 1.0, 0, 0);
	glRotatef(angY, 0, 1.0, 0);
	glRotatef(angZ, 0, 0, 1.0);

	glutWireTorus(10.0, 20.0, 20, 40);

	glPopMatrix();

	glutSwapBuffers();
}


void resizeWindow(GLsizei w, GLsizei h)
{
	if ( h == 0 ) h = 1;

	glViewport(0, 0, w, h);

	fAspect = (GLfloat)w/(GLfloat)h;

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	gluPerspective(30,fAspect,0.5,500);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	gluLookAt(100,30,100, 0,0,0, 0,1,0);
}

void AnimacaoX(int value)
{
    if (angZ == 30.0f && angY == 30.0f) {
        ++angX;

        glutPostRedisplay();
        glutTimerFunc(60,AnimacaoX,1);
    }
}

void AnimacaoY(int value)
{
    if (angZ == 30.0f && angX == 0.0f) {
        ++angY;

        if( angY == 30.0f ) {
            glutPostRedisplay();
            glutTimerFunc(60,AnimacaoX,1);
        } else {
            glutPostRedisplay();
            glutTimerFunc(60,AnimacaoY,1);
        }

    }
}

void AnimacaoZ(int value)
{
    if (angY == 0.0f && angX == 0.0f) {
        ++angZ;

        if( angZ == 30.0f ) {
            glutPostRedisplay();
            glutTimerFunc(60,AnimacaoY,1);
        } else {
            glutPostRedisplay();
            glutTimerFunc(60,AnimacaoZ,1);
        }
    }
}

void restartAnimacao() {
    if (angX == 30.0f && angY == 30.0f && angZ == 30.0f) {
        angX = 0.0f;
        angY = 0.0f;
        angZ = 0.0f;
        glutPostRedisplay();
        glutTimerFunc(60,AnimacaoZ,1);
    }

    glutPostRedisplay();
    glutTimerFunc(60,restartAnimacao,1);
}

void Inicializa(void)
{   
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	angX = 0.0f;
	angY = 0.0f;
	angZ = 0.0f;
}

int main(int argc, char **argv) 
{
   

    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB); 

	glutInitWindowPosition(0,0); 

	glutInitWindowSize(600,500); 

	glutCreateWindow("Exercicio 2");

	glutDisplayFunc(Desenha);

	glutReshapeFunc(resizeWindow);

	glutTimerFunc(60, AnimacaoZ, 1);

	glutTimerFunc(60, restartAnimacao, 1);

	Inicializa();

	glutMainLoop();

	return 0;
}
