// Bibliotecas             Análise para 10 usuários criados no programa:              O         Omega                                             
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cstring>
using namespace std;

// Array do vetor
typedef struct
{
    char nome[50], login[50], senha[50];                                   //         3            3
} Usuario;

// Defindido variáveis globais
int a, b, num_componentes = 10;                                            //         3            3
char in_nome;                                                              //         1            1

Usuario* criarUsuario(Usuario *vetor)// Menu criar usuario                 
{
    a=-1; // Variável que indica se ainda há espaço alocado                           1            1
    for (int cont = 0; cont < num_componentes; cont++){ // Busca por uma posição     10            1
    // vazia no vetor
        if (*vetor[cont].nome == NULL)                                     //        10            1 
        {
            a = cont; // Salva a posição vazia na variável a e "para" o for           1            1
            break;                                                              //    1            1
        }
    }
    if (a==-1){ // Condição para alocar mais espaço na memoria                        1            0
        a = num_componentes; // salva a ultima posição ainda sem informação para      1            0
        // criação do usuário
        num_componentes = num_componentes * 2; // Dobra a variável que indica o       2            0
        // maximo de espaços da memoria
        vetor = (Usuario *) realloc(vetor, num_componentes * sizeof(Usuario)); //     1            0
        // realoca o vetor com o dobro da memoria anterior ~^
        printf("Vetor realocado. Posições: %d\n", num_componentes);
    }
    printf("Criar usuário:\n");
    printf("Nome:");                                                    
    scanf(" %s", &vetor[a].nome); // Entrada do usuário para o nome                   1            1
    printf("Login:");
    scanf(" %s", &vetor[a].login); // Entrada do usuário para o login                 1            1
    printf("Senha:");
    scanf(" %s", &vetor[a].senha); // Entrada do usuário para a senha                 1            1
    printf("Usuário criado com sucesso!\n");
    printf("Digite algo para continuar");
    scanf(" %s", &b); // Aguarda entrada para continuar o programa (linux)            1            1
    system("clear");  // Limpa a tela                                                 1            1
    return vetor; // retorna o vetor com as alterações feitas                         1            1
}

void alterarUsuario(Usuario *vetor) // Função alterar usuário
{
    printf("Aleterar usuário:\n");
    printf("Digite o nome a ser alterado:");
    scanf(" %s", &in_nome); // Entrada do usuário para o nome a ser editado           1            1
    a = -1;                 // Variável que indica se o usuário existe                1            1
    for (int cont = 0; cont < num_componentes; cont++){ // Busca pela posição         10           10
    // em que o nome está registrado
        if (*vetor[cont].nome == in_nome)                                      //     10           10
        {
            a = cont; // Salva a posição do "nome" na variável "a"                    10           1
        }
    }
    if (a == -1){// Condição se o usuário não for encontrado                          1            0
        printf("usuário não encontrado\n");
    }
    else{// Condição se o usuário foi encontrado
        printf("Editando usuario %s:\n", &vetor[a].nome); // Mostra o nome do usuário 
        // que esta senndo editado
        printf("Nome:");
        scanf(" %s", &vetor[a].nome); // Entrada do usuário para o nome               1            1
        printf("Login:");
        scanf(" %s", &vetor[a].login); // Entrada do usuário para o login             1            1
        printf("Senha:");
        scanf(" %s", &vetor[a].senha); // Entrada do usuário para a senha             1            1
        printf("Usuário editado com sucesso\n");
    }
    printf("Digite algo para continuar");
    scanf(" %s", &b); // Aguarda entrada para continuar o programa (linux)            1            1
    system("clear");  // limpa a tela                                                 1            1
}

void removerUsuario(Usuario *vetor) // Função Remover usuário
{
    printf("Remover usuário\n");
    printf("Digite o nome a ser removido:");
    scanf(" %s", &in_nome); // Entrada do usuário para o nome a ser removido          1            1
    a = -1;                 // Variável que indica se o usuário existe                1            1
    for (int cont = 0; cont < num_componentes; cont++){ //Busca pela posição em que   10           10
    // o nome está registrado
        if (*vetor[cont].nome == in_nome)                                  //         10           10
        {
            a = cont; //Salva a posição do "nome" na variável "a"                     10           1
        }
    }
    if (a == -1){ // Condição se o usuário não for encontrado                         1            1
        printf("usuário não encontrado\n");
    }
    else{// condição se o usuário foi encontrado
        memset(&vetor[a], 0, sizeof(Usuario)); // função que limpa a memoria da       1            1
        // posição selecionada
        printf("Usuário removido com sucesso!\n");
    }
    printf("Digite algo para continuar");
    scanf(" %s", &b); // Aguarda entrada para continuar o programa (linux)            1            1
    system("clear");  // Limpa a tela                                                 1            1
}

void mostrarUsuario(Usuario *vetor) // Função Mostrar usuários registrados
{
    printf("Usuários cadastrados:\n");
    for (int cont = 0; cont < num_componentes; cont++){ //Busca todos usuarios        10           10
    // registrados
        if (*vetor[cont].nome != NULL)                                   //           10           10
        { // Todas posições com algum registro é printada na tela
            printf("%d:Nome: %s, Login:%s, Senha:%s\n", 
            cont,
            vetor[cont].nome, 
            vetor[cont].login, 
            vetor[cont].senha);
        }
    }
    printf("Digite algo para continuar");
    scanf(" %s", &b); // Aguarda entrada para continuar o programa (linux             1            1
    system("clear");  // Limpa a tela                                                 1            1
}

// Programa principal
int main(void)
{
    // Definindo o ponteiro v
    Usuario *v;                                                                //     1            1
    // Defindido variáveis
    int i;                                                                  //        1            1
    // Alocando Espaço
    v = (Usuario *)malloc(sizeof(Usuario) * num_componentes);                 //      1            1
    bool run = true; // Condição que mantem o loop principal "rodando"                1            1
    while (run) // Loop principal                                                     n            n
    {
        // Menu principal
        system("clear"); // Limpa a tela
        printf("Main Menu:\n");
        printf("1 - Criar usuário\n");
        printf("2 - Alterar usuário\n");
        printf("3 - Remover usuário\n");
        printf("4 - Mostrar todos usuários\n");
        printf("5 - Sair\n");
        printf("Digite uma opção:");
        scanf(" %i", &i); // Entrada do usuário (opção do menu)                       1           1
        system("clear");  // Limpa a tela
        switch (i){
            case 1:                                                 //                1           1
                v = criarUsuario(v); // Função criar usuário
                break;                                                 //             1           1
            case 2:                                                  //               1           1
                alterarUsuario(v); // Função alterar usuário
                break;                                                  //            1           1
            case 3:                                                    //             1           1
                removerUsuario(v); // Função remover usuário
                break;                                                //              1           1
            case 4:                                                     //            1           1
                mostrarUsuario(v); // Função mostrar usuários
                break;                                                //              1           1
            case 5:                                                   //              1           1
                run = false; // Condição para sair do loop principal                  1           1
                break;                                                  //            1           1
            default:                                                      //          1           1
                break;                                                   //           1           1
        }
    }
}