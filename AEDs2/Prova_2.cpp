#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <string> 
using namespace std;

/*typedef struct
{
    char palavra[50];
} Frase;

// Defindido variáveis globais
int a, b, num_componentes = 10;                                            
char in_nome;   

Frase* adicionarPalavra(Frase *vetor)// Menu criar usuario                 
{
    a=-1; // Variável que indica se ainda há espaço alocado                          
    for (int cont = 0; cont < num_componentes; cont++){ // Busca por uma posição     
    // vazia no vetor
        if (*vetor[cont].palavra == NULL)                                    
        {
            a = cont; // Salva a posição vazia na variável a e "para" o for         
            break;                                                              
        }
    }
    if (a==-1){ // Condição para alocar mais espaço na memoria                       
        a = num_componentes; // salva a ultima posição ainda sem informação para      
        // criação do usuário
        num_componentes = num_componentes * 2; // Dobra a variável que indica o     
        // maximo de espaços da memoria
        vetor = (Frase *) realloc(vetor, num_componentes * sizeof(Frase));      
        // realoca o vetor com o dobro da memoria anterior ~^
        printf("Vetor realocado. Posições: %d\n", num_componentes);
    }
    printf("Adicionar palavra:\n");                                                  
    scanf(" %s", &vetor[a].palavra); // Entrada do usuário para o nome                                             1
    printf("Palavra adicionada com sucesso!\n");
    printf("Digite algo para continuar");
    scanf(" %s", &b); // Aguarda entrada para continuar o programa (linux)            
    system("clear");  // Limpa a tela                                                 
    return vetor; // retorna o vetor com as alterações feitas                        
}

void removerPalavra(Frase *vetor) // Função Remover usuário
{
    a=-1;
    for (int cont = 0; cont < num_componentes; cont++){ //Busca pela posição em que   
    // o nome está registrado
        if (*vetor[cont].palavra == NULL)                                  
        {
            a = cont; //Salva a posição do "nome" na variável "a" 
            break;               
        }
    }
    memset(&vetor[a-1], 0, sizeof(Frase)); // função que limpa a memoria da      
    // posição selecionada
    printf("Palavra removida com sucesso!\n");
    printf("Digite algo para continuar");
    scanf(" %s", &b); // Aguarda entrada para continuar o programa (linux)           
    system("clear");  // Limpa a tela                                               
}

void mostrarFrase(Frase *vetor) // Função Mostrar usuários registrados
{
    printf("Palavras registradas:\n");
    for (int cont = 0; cont < num_componentes; cont++){ //Busca todos usuarios        
    // registrados
        if (*vetor[cont].palavra!= NULL)                                  
        { // Todas posições com algum registro é printada na tela
            printf("%s ", 
            vetor[cont].palavra);
        }
    }
    printf("\n Digite algo para continuar");
    scanf("%s", &b); // Aguarda entrada para continuar o programa (linux             
    system("clear");  // Limpa a tela                                               
}

int main(void){
    // Definindo o ponteiro v
    Frase *v;                                                               
    // Defindido variáveis
    int i;                                                                 
    // Alocando Espaço
    v = (Frase *)malloc(sizeof(Frase) * num_componentes);  
    bool run = true; // Condição que mantem o loop principal "rodando"             
    while (run) // Loop principal                                              
    {
        // Menu principal
        system("clear"); // Limpa a tela
        printf("Main Menu:\n");
        printf("1 - Adicionar palavra\n");
        printf("2 - Remover ultima palavra\n");
        printf("3 - Mostrar palavras\n");
        printf("4 - Sair\n");
        printf("Digite uma opção:");
        scanf(" %i", &i); // Entrada do usuário (opção do menu)                      
        system("clear");  // Limpa a tela
        switch (i){
            case 1:                                                
                v = adicionarPalavra(v); // Função adicionar palavra
                break;                                                
            case 2:                                                 
                removerPalavra(v); // Função remover palavra
                break;                                                                                               //              1           1
            case 3:                                                    
                mostrarFrase(v); // Função mostrar usuários
                break;                                                
            case 4:                                                   
                run = false; // Condição para sair do loop principal    
                break;                                                  
            default:                                                     
                break;                                                  
        }
    }

}*/

/*
typedef struct
{
    int senha[50];
} Fila;

// Defindido variáveis globais
int a, b, num_componentes = 10;                                            
char in_nome;   

Fila* AdicionarSenha(Fila *vetor)// Menu criar usuario                 
{
    a=-1; // Variável que indica se ainda há espaço alocado                          
    for (int cont = 0; cont < num_componentes; cont++){ // Busca por uma posição     
    // vazia no vetor
        if (*vetor[cont].senha == NULL)                                    
        {
            a = cont; // Salva a posição vazia na variável a e "para" o for         
            break;                                                              
        }
    }
    if (a==-1){ // Condição para alocar mais espaço na memoria                       
        a = num_componentes; // salva a ultima posição ainda sem informação para      
        // criação do usuário
        num_componentes = num_componentes * 2; // Dobra a variável que indica o     
        // maximo de espaços da memoria
        vetor = (Fila *) realloc(vetor, num_componentes * sizeof(Fila));      
        // realoca o vetor com o dobro da memoria anterior ~^
        printf("Vetor realocado. Posições: %d\n", num_componentes);
    }
    if (a == 0){
        printf("%d ", &vetor[a].senha);
        &vetor[a].senha = 0;
    }else{
        printf("%d ", &vetor[a].senha);                                                 
        &vetor[a].senha = &vetor[a-1].senha + 1; // Entrada do usuário para o nome
    }
    printf("Senha adicionada com sucesso!\n");
    printf("Digite algo para continuar");
    scanf(" %s", &b); // Aguarda entrada para continuar o programa (linux)            
    system("clear");  // Limpa a tela                                                 
    return vetor; // retorna o vetor com as alterações feitas                        
}

void removerSenha(Fila *vetor) // Função Remover usuário
{
    memset(&vetor[0], 0, sizeof(Fila)); // função que limpa a memoria da      
    // posição selecionada
    a=-1;
    for (int cont = 0; cont < num_componentes; cont++){ //Busca pela posição em que   
    // o nome está registrado
        *vetor[cont].senha = *vetor[cont+1].senha;           
    }
    printf("Palavra removida com sucesso!\n");
    printf("Digite algo para continuar");
    scanf(" %s", &b); // Aguarda entrada para continuar o programa (linux)           
    system("clear");  // Limpa a tela                                               
}

void mostrarFila(Fila *vetor) // Função Mostrar usuários registrados
{
    printf("Palavras registradas:\n");
    for (int cont = 0; cont < num_componentes; cont++){ //Busca todos usuarios        
    // registrados
        if (*vetor[cont].senha!= NULL)                                  
        { // Todas posições com algum registro é printada na tela
            printf("%d ", 
            vetor[cont].senha);
        }
    }
    printf("\n Digite algo para continuar");
    scanf("%s", &b); // Aguarda entrada para continuar o programa (linux             
    system("clear");  // Limpa a tela                                               
}

int main(void){
    // Definindo o ponteiro v
    Fila *v;                                                               
    // Defindido variáveis
    int i;                                                                 
    // Alocando Espaço
    v = (Fila *)malloc(sizeof(Fila) * num_componentes);  
    bool run = true; // Condição que mantem o loop principal "rodando"             
    while (run) // Loop principal                                              
    {
        // Menu principal
        system("clear"); // Limpa a tela
        printf("Main Menu:\n");
        printf("1 - Retirar senha\n");
        printf("2 - Chamar cliente\n");
        printf("3 - Mostrar Fila de espera\n");
        printf("4 - Sair\n");
        printf("Digite uma opção:");
        scanf(" %i", &i); // Entrada do usuário (opção do menu)                      
        system("clear");  // Limpa a tela
        switch (i){
            case 1:                                                
                v = AdicionarSenha(v); // Função adicionar palavra
                break;                                                
            case 2:                                                 
                removerSenha(v); // Função remover palavra
                break;                                                                                               //              1           1
            case 3:                                                    
                mostrarFila(v); // Função mostrar usuários
                break;                                                
            case 4:                                                   
                run = false; // Condição para sair do loop principal    
                break;                                                  
            default:                                                     
                break;                                                  
        }
    }

}*/
