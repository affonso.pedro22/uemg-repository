// Questão 1
////////////////////////////////////////////////////////////////

//A)
/*void int retornaMaiorValor(int *array, int n)
{
    int maior = array[0];
    for(int i = 0;i<n; i++)
    {
        if(array[i]>maior)
        {
            maior = array[i];
        }
    }
}*/
// Resposta: f(n) = 3 + 4n
// 3 -> Inicialização
// 4n -> cresce em funcao do n
////////////////////////////////////////////////////////////////

//B)
/*void ImprimeMatrizQuadrada(int*matriz, int n)
{
    for(int x = 0;x<n; x++)
    {
        for(int y = 0; y<n; y++)
        {
            printf("%d", matriz[x][y]);
        }
    }
}*/
// Respsta: f(n) = n² + 2n + 2
// 2 -> inicialização do primeiro for
// 2n -> inicialização do segundo for multiplicado pelo n que é a quantidade de vezes q vai rodar
// n² -> n * n pois tem um for dentro do outro
////////////////////////////////////////////////////////////////

//C)
/*void OrdenaVetor(int*array, int n)
{
    for(int x = 0;x<n-1; x++)
    {
        for(int y = x+1; y<n; y++)
        {
            if (array[x]>array[y])
            {
                int aux = array[x];
                array[x] = array[y];
                array[y] = aux;
                x=0;
                break;
            }
        }
    }
}*/
// Resposta: n² + 9n + 2
// 2 -> inicialização do  primeiro for
// 9n -> inicialização do segundo for (2) + as 7 linhas (incluindo o if) multiplicado pelo n, que é a quantidade de vezes que vai rodar
// n² -> n * n pois tem um for dentro do outro
////////////////////////////////////////////////////////////////

//Questão 2 -- RODANDO

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cstring>
using namespace std;

// Array do vetor
typedef struct
{
    int a, b;    
} Var;

// Defindido variáveis globais
int x, y, num_componentes = 2;                                       

void *trocarValores(Var *vetor) // Função trocaValores
{
    int i,t;// Variaveis auxiliares para salvar temporariamente os valores a serem trocados.
    i = vetor[0].a; // i recebe a
    t = vetor[0].b; // t recebe b
    free(vetor); // limpa a memoria
    printf("Limpando: A=%d B=%d\n", vetor[0].a, vetor[0].b);
    vetor[0].a = t; // a recebe t
    vetor[0].b = i; // b recebe i
    return vetor; // retorna o valor do vetor atualizado
}

int main(void)
{
    // Definindo o ponteiro v
    Var *v;                                                                                                                           
    // Alocando Espaço
    v = (Var *)malloc(sizeof(Var) * num_componentes);

    printf("Digite o valor da variavel A:");                                                    
    scanf(" %d", &v[x].a); // Entrada do usuário para o a               
    printf("Digite o valor da variavel B:");
    scanf(" %d", &v[x].b); // Entrada do usuário para o b     
    printf("Digite algo para continuar");
    scanf(" %s", &y); // Aguarda entrada para continuar o programa (linux)            
    system("clear");  // Limpa a tela                                                     

    printf("A=%d B=%d\n", v[0].a, v[0].b); // print a e b
    
    printf("Digite algo para chamar a funçao trocarValores: \n");
    scanf(" %s", &y); // Aguarda entrada para continuar o programa (linux)   
    trocarValores(v); // função que inverte os valores
    printf("A=%d B=%d\n", v[0].a, v[0].b); // mesmo print a e b
}
////////////////////////////////////////////////////////////////

//Questão 3
////////////////////////////////////////////////////////////////
// Array do vetor
/*typedef struct
{
    char nome[51];
    int idade;
    char cpf[15];
}Pessoa;

int quantidadePessoa=10; // variavel global
////////////////////////////////////////////////////////////////

//A)
Pessoa * AlocarEspaco(int *quantidadePessoa)// Alocar espaço                
{
    // Definindo o ponteiro pessoa
    Pessoa *pessoa;
    // Alocando Espaço                                                             
    pessoa = (Pessoa *)malloc(sizeof(Pessoa) * quantidadePessoa); 
}
////////////////////////////////////////////////////////////////

//B)
Pessoa * RelocarEspaco(Pessoa * pessoas, int *quantidadePessoa)// Menu criar usuario                 
{
    quantidadePessoa = quantidadePessoa * 2; // Dobra a variável que indica o maximo de espaços da memoria
    quantidadePessoa = (Pessoa *) realloc(pessoas, quantidadePessoa * sizeof(Pessoa));     
    // realoca o vetor com o dobro da memoria anterior ~^
    printf("Vetor realocado. Posições: %d\n", quantidadePessoa);
    return pessoas; // retorna o vetor atualizado
}
////////////////////////////////////////////////////////////////

//C
void ListarPessoas(Pessoa *pessoas, int quantidadePessoas) // Função Mostrar Pessoas registradas
{
    printf("Pessoas cadastradas:\n");
    for (int cont = 0; cont < quantidadePessoas; cont++){ //Busca todas pessoas      
    // registradas
        if (*pessoas[cont].nome != NULL)                                 
        { // Todas posições com algum registro é printada na tela
            printf("%d: Nome: %s, Idade:%s, CPF:%s\n", 
            cont,
            pessoas[cont].nome, 
            pessoas[cont].idade, 
            pessoas[cont].cpf);
        }
    }                                            
}*/